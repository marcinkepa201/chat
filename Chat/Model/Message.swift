//
//  Message.swift
//  Chat
//
//  Created by Marcin Kępa on 17/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import Foundation

struct Message {
    let sender: String
    let body: String
}
