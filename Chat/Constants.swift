//
//  Constants.swift
//  Chat
//
//  Created by Marcin Kępa on 17/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

struct K {
    static let welcome = "Welcome in Chat🙂"
    static let appName = "Chat 🙂"
    static let cellIdentifier = "ReusableCell"
    static let cellNibName = "MessageCell"
    static let registerSegue = "RegisterToChatSegue"
    static let loginSegue = "LoginToChatSegue"
    
    struct BrandColors {
        static let purple = "BrandPurple"
        static let lightPurple = "BrandLightPurple"
        static let blue = "BrandBlue"
        static let lighBlue = "BrandLightBlue"
    }
    
    struct FStore {
        static let collectionName = "messages"
        static let senderField = "sender"
        static let bodyField = "body"
        static let dateField = "date"
    }
}
