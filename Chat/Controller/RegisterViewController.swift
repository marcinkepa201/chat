//
//  RegisterViewController.swift
//  Chat
//
//  Created by Marcin Kępa on 15/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBAction func registerPressed(_ sender: UIButton) {
        
        if let email = emailTextField.text, let password = passwordTextField.text{
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                if let e = error {
                    self.alert(title: "Register error!", message: e.localizedDescription)
                }else {
                    self.performSegue(withIdentifier: K.registerSegue, sender: self )
                }
            }
            
        }
    }
    
    func alert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
}
